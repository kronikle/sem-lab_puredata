# Manuale Minimo di Sopravvivenza a Pure Data
    | ----[ seminario laboratoriale: pure data ]---- |
    | ----[     a cura di k_, Unit hacklab     ]---- |
    | ----[                                    ]---- |
    | ----[        Repository del corso        ]---- |

## Seminario n°1 24/03/21
#### Argomenti trattati:
- Storia
- Installazione
- Setting iniziale
- Box, Collegamenti, Trigger
- SubPatches / Abstractions
- Counter
- Sequencer
- Primo Oscillatore 
